#!/usr/bin/env python


__version__ = '1.0'
__author__ = 'Tomek Roman'
__email__ = 'Gombkatom@gmail.com'
__copyright__ = "Copyright (c) 2020 Tomasz Roman"

def print_my_name(name: str) -> None:
    print("My name is", name)


print_my_name("Tomek")


def add_3_numb(a: int,b: int,c: int) -> None:
    print(a+b+c)

add_3_numb(6,5,4)

def hello_world() -> str:
    return "Hello World"

print(hello_world())

def sub_2_numb(a: int,b: int) -> None:
    print(a-b)

sub_2_numb(10,8)

class Object: pass

def create_object() -> Object:
    return Object()

thing = create_object()
print(isinstance(thing,Object))