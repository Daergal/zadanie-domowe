#!/usr/bin/env python


__version__ = '1.0'
__author__ = 'Tomek Roman'
__email__ = 'Gombkatom@gmail.com'
__copyright__ = "Copyright (c) 2020 Tomasz Roman"

objects = [
    [1],
    [2],
    [3]
]

objects = list()
for i in range(100, 116):
    objects.append(i)

print(objects)


float_list = [1.12, 1.71, 2.53, 5, 6.43]
for i in float_list:
    print(i)