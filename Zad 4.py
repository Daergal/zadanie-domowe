#!/usr/bin/env python


__version__ = '1.0'
__author__ = 'Tomek Roman'
__email__ = 'Gombkatom@gmail.com'
__copyright__ = "Copyright (c) 2020 Tomasz Roman"

name = input("Podaj imie: ")
surname = input("Podaj nazwisko: ")
age = input("Podaj wiek: ")
print(name, surname, age)

def take_number():
    taken_number = input("Podaj losowa liczbe: ")
    print(int(taken_number))

take_number()

class Tulip:
    def __init__(self, color: str, look: str):
        print("Ten kwiat jest koloru", color, "i moim zdaniem wygląda", look)

color = input("Podaj kolor Tulipanu: ")
look = input("Jak wyglada ten kwiat: ")

tulip = Tulip(color, look)