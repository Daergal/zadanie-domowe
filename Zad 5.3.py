#!/usr/bin/env python


__version__ = '1.0'
__author__ = 'Tomek Roman'
__email__ = 'Gombkatom@gmail.com'
__copyright__ = "Copyright (c) 2020 Tomasz Roman"

class Cube:
    def __init__(self, capacity: int):
        self.capacity: int = capacity


    def __eq__(self, other):
        return self.capacity == other.capacity

    def __lt__(self, other):
        return self.capacity < other.capacity

    def __gt__(self, other):
        return self.capacity > other.capacity

    def __le__(self, other):
        return self.capacity <= other.capacity

    def __ge__(self, other):
        return self.capacity >= other.capacity

    def __ne__(self, other):
        return self.capacity != other.capacity



a = Cube(8 ** 3)
b = Cube(15 ** 3)

print(format(a == b))
print(format(a < b))
print(format(a > b))
print(format(a <= b))
print(format(a >= b))
print(format(a != b))