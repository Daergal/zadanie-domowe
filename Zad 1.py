#!/usr/bin/env python


__version__ = '1.0'
__author__ = 'Tomek Roman'
__email__ = 'Gombkatom@gmail.com'
__copyright__ = "Copyright (c) 2020 Tomasz Roman"


class Smartphone: pass

class Xiaomi(Smartphone): pass

class Redmi(Xiaomi): pass
