#!/usr/bin/env python


__version__ = '1.0'
__author__ = 'Tomek Roman'
__email__ = 'Gombkatom@gmail.com'
__copyright__ = "Copyright (c) 2020 Tomasz Roman"

class Rectangle:
    def __init__(self, side_length: int):
        self.side_length: int = side_length


    def __eq__(self, other):
        return self.side_length == other.side_length

    def __lt__(self, other):
        return self.side_length < other.side_length

    def __gt__(self, other):
        return self.side_length > other.side_length

    def __le__(self, other):
        return self.side_length <= other.side_length

    def __ge__(self, other):
        return self.side_length >= other.side_length

    def __ne__(self, other):
        return self.side_length != other.side_length


a = Rectangle(8)
b = Rectangle(10)

print(format(a == b))
print(format(a < b))
print(format(a > b))
print(format(a <= b))
print(format(a >= b))
print(format(a != b))