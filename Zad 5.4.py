#!/usr/bin/env python


__version__ = '1.0'
__author__ = 'Tomek Roman'
__email__ = 'Gombkatom@gmail.com'
__copyright__ = "Copyright (c) 2020 Tomasz Roman"


class Tower:
    def __init__(self, attack: int, defense: int):
        self.attack: int = attack
        self.defense: int = defense

    def __eq__(self, other):
        return self.attack == other.attack and self.defense == other.defense

    def __lt__(self, other):
        return self.attack < other.attack and self.defense < other.defense

    def __gt__(self, other):
        return self.attack > other.attack and self.defense > other.defense

    def __le__(self, other):
        return self.attack <= other.attack and self.defense <= other.defense

    def __ge__(self, other):
        return self.attack >= other.attack and self.defense >= other.defense

    def __ne__(self, other):
        return self.attack != other.attack and self.defense != other.defense


class Army(Tower):
    def __init__(self, attack: int, defense: int):
        self.attack: int = attack
        self.defense: int = defense
        super().__init__(attack, defense)

    def __eq__(self, other):
        return self.attack == other.attack and self.defense == other.defense

    def __lt__(self, other):
        return self.attack < other.attack and self.defense < other.defense

    def __gt__(self, other):
        return self.attack > other.attack and self.defense > other.defense

    def __le__(self, other):
        return self.attack <= other.attack and self.defense <= other.defense

    def __ge__(self, other):
        return self.attack >= other.attack and self.defense >= other.defense

    def __ne__(self, other):
        return self.attack != other.attack and self.defense != other.defense


class MageTower(Army):
    def __init__(self, attack: int, defense: int):
        self.attack: int = attack
        self.defense: int = defense
        super().__init__(attack, defense)

    def __eq__(self, other):
        return self.attack == other.attack and self.defense == other.defense

    def __lt__(self, other):
        return self.attack < other.attack and self.defense < other.defense

    def __gt__(self, other):
        return self.attack > other.attack and self.defense > other.defense

    def __le__(self, other):
        return self.attack <= other.attack and self.defense <= other.defense

    def __ge__(self, other):
        return self.attack >= other.attack and self.defense >= other.defense

    def __ne__(self, other):
        return self.attack != other.attack and self.defense != other.defense


class Mage(MageTower):
    def __init__(self, attack: int, defense: int):
        self.attack: int = attack
        self.defense: int = defense
        super().__init__(attack, defense)

    def __eq__(self, other):
        return self.attack == other.attack and self.defense == other.defense

    def __lt__(self, other):
        return self.attack < other.attack and self.defense < other.defense

    def __gt__(self, other):
        return self.attack > other.attack and self.defense > other.defense

    def __le__(self, other):
        return self.attack <= other.attack and self.defense <= other.defense

    def __ge__(self, other):
        return self.attack >= other.attack and self.defense >= other.defense

    def __ne__(self, other):
        return self.attack != other.attack and self.defense != other.defense


class GolemFactory(Army):
    def __init__(self, attack: int, defense: int):
        self.attack: int = attack
        self.defense: int = defense
        super().__init__(attack, defense)

    def __eq__(self, other):
        return self.attack == other.attack and self.defense == other.defense

    def __lt__(self, other):
        return self.attack < other.attack and self.defense < other.defense

    def __gt__(self, other):
        return self.attack > other.attack and self.defense > other.defense

    def __le__(self, other):
        return self.attack <= other.attack and self.defense <= other.defense

    def __ge__(self, other):
        return self.attack >= other.attack and self.defense >= other.defense

    def __ne__(self, other):
        return self.attack != other.attack and self.defense != other.defense


class Golem(GolemFactory):
    def __init__(self, attack: int, defense: int):
        self.attack: int = attack
        self.defense: int = defense
        super().__init__(attack, defense)

    def __eq__(self, other):
        return self.attack == other.attack and self.defense == other.defense

    def __lt__(self, other):
        return self.attack < other.attack and self.defense < other.defense

    def __gt__(self, other):
        return self.attack > other.attack and self.defense > other.defense

    def __le__(self, other):
        return self.attack <= other.attack and self.defense <= other.defense

    def __ge__(self, other):
        return self.attack >= other.attack and self.defense >= other.defense

    def __ne__(self, other):
        return self.attack != other.attack and self.defense != other.defense


golem = Golem(7, 10)
mage = Mage(11, 8)

print(format(golem.attack == mage.attack))
print(format(golem.attack > mage.attack))
print(format(golem.attack < mage.attack))
print(format(golem.attack <= mage.attack))
print(format(golem.attack >= mage.attack))
print(format(golem.attack != mage.attack))
print(format(golem.defense == mage.defense))
print(format(golem.defense > mage.defense))
print(format(golem.defense < mage.defense))
print(format(golem.defense <= mage.defense))
print(format(golem.defense >= mage.defense))
print(format(golem.defense != mage.defense))
